#include <stdlib.h>
#include <time.h>
#include "HashTable.h"


int main(int argc, char **argv)
{
    Dictionary_Type *Dict;
    WordList_Type *Corpus;
    char *corpusname, *queriesname, *resultsname;
    FILE *corpusfile, *queriesfile, *resultsfile;
    unsigned int nqueries;


    if (argc != 5)
    {
        printf("Run as:\n");
        printf("%s <corpus infile> <queries infile> <results outfile> <# of suggestions>\n", argv[0]);
        return 0;
    }

    corpusname = argv[1];
    queriesname = argv[2];
    resultsname = argv[3];
    nqueries = strtol(argv[4], NULL, 10);

    // try to open the corpus infile read-only
    corpusfile = fopen(corpusname, "r");
    if (corpusfile == NULL)
    {
        fprintf(stderr, "\nCoulnd't open file for input: %s\n", corpusname);
        return EXIT_FAILURE;
    }

    // try to open the queries infile read-only
    queriesfile = fopen(queriesname, "r");
    if (queriesfile == NULL)
    {
        fprintf(stderr, "\nCoulnd't open file for input: %s\n", queriesname);
        fclose(corpusfile);
        return EXIT_FAILURE;
    }

    // try to open the results outfile for writing and appending
    resultsfile = fopen(resultsname, "w");
    if (resultsfile == NULL)
    {
        fprintf(stderr, "\nCoulnd't open file for output: %s\n", resultsname);
        fclose(corpusfile);
        fclose(queriesfile);
        return EXIT_FAILURE;
    }

    srand(time(NULL));
    Dict = StartDictionary(1000000);

    Corpus = TextFileToWordList(corpusfile);
    Corpus = CaseNormalizeWordList(Corpus);

    Dict = AddWordListToDictionary(Dict, Corpus);
    Dict = SetAndSortAllFrequenciesInDictionary(Dict);

    DeleteWordList(Corpus); // free this early

    QuerySuggestionsFromTextFile(Dict, queriesfile, nqueries, resultsfile);

    DeleteDictionary(Dict);
    fclose(corpusfile);
    fclose(queriesfile);
    fclose(resultsfile);
    return EXIT_SUCCESS;
}
