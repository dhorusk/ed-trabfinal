#ifndef _CROSSCLOCK_H_
#define _CROSSCLOCK_H_

#include <stdint.h>
#include <math.h>
#ifdef _WIN32
    #include <windows.h>
#else
    #include <time.h>
#endif // _WIN32

enum ClockResolutions {
    CLOCK_S,
    CLOCK_MS,
    CLOCK_US,
    CLOCK_NS,
};


#ifdef _WIN32

uint64_t ClockNow(int clock_type)
{
    LARGE_INTEGER timeLI, freqLI;
    double freq;


    QueryPerformanceCounter(&timeLI);
    QueryPerformanceFrequency(&freqLI);

    switch (clock_type)
    {
        case CLOCK_S:
            freq = (double)freqLI.QuadPart;
            break;
        case CLOCK_MS:
            freq = (double)freqLI.QuadPart / 1000.0f;
            break;
        case CLOCK_US:
            freq = (double)freqLI.QuadPart / 1000000.0f;
            break;
        case CLOCK_NS:
            freq = (double)freqLI.QuadPart / 1000000000.0f;
            break;
        default:
            freq = (double)freqLI.QuadPart / 1000.0f;
            break;
    }

    return floor(timeLI.QuadPart / freq);
}

#else

double TimeSpecToS(struct timespec* ts)
{
    return (double)ts->tv_sec + (double)ts->tv_nsec / 1000000000.0f;
}

double TimeSpecToMs(struct timespec* ts)
{
    return (double)ts->tv_sec * 1000.0f + (double)ts->tv_nsec / 1000000.0f;
}

double TimeSpecToUs(struct timespec* ts)
{
    return (double)ts->tv_sec * 1000000.0f + (double)ts->tv_nsec / 1000.0f;
}

double TimeSpecToNs(struct timespec* ts)
{
    return (double)ts->tv_sec * 1000000000.0f + (double)ts->tv_nsec;
}

uint64_t ClockNow(int clock_type)
{
    struct timespec time;


    clock_gettime(CLOCK_MONOTONIC, &time);

    switch (clock_type)
    {
        case CLOCK_S:
            return floor(TimeSpecToS(&time));
        case CLOCK_MS:
            return floor(TimeSpecToMs(&time));
        case CLOCK_US:
            return floor(TimeSpecToUs(&time));
        case CLOCK_NS:
            return floor(TimeSpecToNs(&time));
        default:
            return floor(TimeSpecToMs(&time));
    }
}

#endif // _WIN32


#endif // _CROSSCLOCK_H_
