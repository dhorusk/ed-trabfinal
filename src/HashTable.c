#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "CrossClock.h"
#include "HashTable.h"
#include "murmur3.h"



/** General purpose functions **/


/* normalize a string to lowercase */
void CaseNormalize(char *str)
{
    uint32_t i, len;


    if (str == NULL)
        return;

    // copy the string as lowercase
    len = strlen(str);
    for (i = 0; i < len; i++)
        str[i] = tolower(str[i]);
}


/* hash a word with 32-bit MurmurHash3 */
uint32_t HashWord(const char *str, uint32_t seed)
{
    uint32_t len, hash[4];


    if (str == NULL)
        return -1;

    // normalize the word's case before hashing
    len = strlen(str);
    MurmurHash3_x86_32(str, len, seed, hash);

    return hash[0];
}



/** WordSuggestion functions **/


/* find a suggestion in a suggestions list, if it exists in there */
WordSuggestion_Type *FindSuggestionInSuggestionsList(WordSuggestion_Type *Sgstn, const char *WordStr)
{
    if (Sgstn == NULL || WordStr == NULL)
        return NULL;

    while (Sgstn != NULL)
    {
        // case insensitive compare
        if (strcasecmp(Sgstn->WordStr, WordStr) == 0)
            break;

        Sgstn = Sgstn->NextSuggestion;
    }

    return Sgstn;
}


/* add a word from a list as a word suggestion */
WordSuggestion_Type *AddWordToWordSuggestions(WordSuggestion_Type *Sgstn, const char *WordStr)
{
    uint32_t len;
    WordSuggestion_Type *tmpSgstn, *tmpLastSgstn;


    if (WordStr == NULL)
        return NULL;

    // check if the word is already in the suggestions list
    tmpSgstn = FindSuggestionInSuggestionsList(Sgstn, WordStr);

    // if the suggestion was already in the suggestions list, count it and return
    if (tmpSgstn != NULL)
    {
        tmpSgstn->Occurrences += 1;
        return Sgstn;
    }

    // if it wasn't, init a new suggestion for the suggestions list
    tmpSgstn = calloc(1, sizeof(WordSuggestion_Type));
    len = strlen(WordStr);
    tmpSgstn->WordStr = calloc(len + 1, sizeof(char)); // len + \0
    strncpy(tmpSgstn->WordStr, WordStr, len);
    tmpSgstn->Occurrences = 1;

    // if it's the first suggestion for this word, just add it
    if (Sgstn == NULL)
    {
        Sgstn = tmpSgstn;
        return Sgstn;
    }

    // if not, find the last suggestion in the suggestions list
    tmpLastSgstn = Sgstn;
    while (tmpLastSgstn->NextSuggestion != NULL)
        tmpLastSgstn = tmpLastSgstn->NextSuggestion;

    // insert the new word in the suggestions list
    tmpLastSgstn->NextSuggestion = tmpSgstn;

    return Sgstn;
}


/* merge the suggestions from 2 sorted independent suggestions lists */
WordSuggestion_Type *MergeSuggestions(WordSuggestion_Type *SgstnPart1, WordSuggestion_Type *SgstnPart2)
{
    WordSuggestion_Type *tmpSgstn, *tmpNewSgstn;


    if (SgstnPart1 == NULL)
        return SgstnPart2;

    if (SgstnPart2 == NULL)
        return SgstnPart1;

    // take the suggestion with the greatest frequency to start the merged list
    if (SgstnPart1->Frequency >= SgstnPart2->Frequency)
    {
        tmpNewSgstn = SgstnPart1;
        SgstnPart1 = SgstnPart1->NextSuggestion;
    }
    else
    {
        tmpNewSgstn = SgstnPart2;
        SgstnPart2 = SgstnPart2->NextSuggestion;
    }

    // while both parts have suggestions, take the ones with greatest frequency
    tmpSgstn = tmpNewSgstn;
    while (SgstnPart1 != NULL && SgstnPart2 != NULL)
    {
        if (SgstnPart1->Frequency >= SgstnPart2->Frequency)
        {
            tmpSgstn->NextSuggestion = SgstnPart1;
            SgstnPart1 = SgstnPart1->NextSuggestion;
        }
        else
        {
            tmpSgstn->NextSuggestion = SgstnPart2;
            SgstnPart2 = SgstnPart2->NextSuggestion;
        }

        tmpSgstn = tmpSgstn->NextSuggestion;
    }

    // get the remaining suggestions from part 1
    while (SgstnPart1 != NULL)
    {
        tmpSgstn->NextSuggestion = SgstnPart1;
        SgstnPart1 = SgstnPart1->NextSuggestion;
        tmpSgstn = tmpSgstn->NextSuggestion;
    }

    // get the remaining suggestions from part 2
    while (SgstnPart2 != NULL)
    {
        tmpSgstn->NextSuggestion = SgstnPart2;
        SgstnPart2 = SgstnPart2->NextSuggestion;
        tmpSgstn = tmpSgstn->NextSuggestion;
    }

    return tmpNewSgstn;
}


/* sort all the suggestions using merge sort */
WordSuggestion_Type *SortSuggestions(WordSuggestion_Type *Sgstn, uint32_t nSgstns)
{
    uint32_t i, nPart1, nPart2;
    WordSuggestion_Type *SgstnPart1, *SgstnPart2, *tmpSgstn;


    if (Sgstn == NULL)
        return NULL;

    if (nSgstns <= 1)
        return Sgstn;

    // calculate the sizes of the split suggestions list
    nPart1 = nSgstns / 2;
    nPart2 = nSgstns - nPart1;

    // get the last suggestion in the first part
    tmpSgstn = Sgstn;
    for (i = 1; tmpSgstn != NULL && i < nPart1; i++) // start from 1
        tmpSgstn = tmpSgstn->NextSuggestion;

    // get the 2 different suggestions lists
    SgstnPart1 = Sgstn;
    SgstnPart2 = tmpSgstn->NextSuggestion;
    tmpSgstn->NextSuggestion = NULL; // make them independent lists

    // sort the 2 different suggestions lists
    SgstnPart1 = SortSuggestions(SgstnPart1, nPart1);
    SgstnPart2 = SortSuggestions(SgstnPart2, nPart2);

    tmpSgstn = MergeSuggestions(SgstnPart1, SgstnPart2);

    return tmpSgstn;
}


/* list all the suggestions for a word */
void DumpSuggestions(WordSuggestion_Type *Sgstn)
{
    WordSuggestion_Type *tmpSgstn;


    if (Sgstn == NULL)
    {
        printf("\tNo suggestions\n");
        return;
    }

    // print all the words in the suggestions list
    tmpSgstn = Sgstn;
    while (tmpSgstn != NULL)
    {
        printf("\t%s: %d, %.9f\n", tmpSgstn->WordStr, tmpSgstn->Occurrences, tmpSgstn->Frequency);
        tmpSgstn = tmpSgstn->NextSuggestion;
    }
}


/* present and format a number of word suggestions */
void PresentWordSuggestions(WordSuggestion_Type *Sgstn, uint32_t nSgstns, FILE *resultsfile)
{
    uint32_t i;
    WordSuggestion_Type *tmpSgstn;


    if (resultsfile == NULL)
        return;

    if (Sgstn == NULL)
    {
        fprintf(resultsfile, "No suggestions\r\n");
        return;
    }

    // print a given number of suggestions
    tmpSgstn = Sgstn;
    for (i = 0; tmpSgstn != NULL && (nSgstns == 0 || i < nSgstns); i++)
    {
        fprintf(resultsfile, "Suggestion: %-20s (%f)\r\n", tmpSgstn->WordStr, tmpSgstn->Frequency);
        tmpSgstn = tmpSgstn->NextSuggestion;
    }
}


/* delete a single word suggestion structure */
void DeleteWordSuggestion(WordSuggestion_Type *Sgstn)
{
    if (Sgstn == NULL)
        return;

    free(Sgstn->WordStr);
    free(Sgstn);
}



/** Word functions **/


/* init a word structure */
Word_Type *StartWord(const char *WordStr)
{
    uint32_t len;
    Word_Type *Word;


    if (WordStr == NULL)
        return NULL;

    // return a new word structure from a word in a word list
    Word = malloc(sizeof(Word_Type));
    len = strlen(WordStr);
    Word->WordStr = calloc(len + 1, sizeof(char)); // len + \0
    strncpy(Word->WordStr, WordStr, len);
    Word->Occurrences = 1;
    Word->WordSuggestions = NULL;
    Word->NextWord = NULL;

    return Word;
}


/* find a word in a bucket, if it exists in there */
Word_Type *FindWordInBucket(Bucket_Type Bkt, const char *WordStr)
{
    Word_Type *tmpWord;

    if (WordStr == NULL)
        return NULL;

    // check if the word exists in the bucket
    tmpWord = Bkt.Words;
    while (tmpWord != NULL)
    {
        // case insensitive compare
        if (strcasecmp(tmpWord->WordStr, WordStr) == 0)
            break;

        tmpWord = tmpWord->NextWord;
    }

    return tmpWord;
}


/* set the frequencies of all the suggestions in the word */
Word_Type *SetAllFrequenciesInWord(Dictionary_Type *Dict, Word_Type *Word)
{
    uint32_t freq_ab, freq_a, freq_b;
    WordSuggestion_Type *tmpSgstn;


    if (Dict == NULL || Word == NULL)
        return NULL;

    // for each existing suggestion, calculate its frequency
    // the formula is: "occurs of b after a" / sqrt( "occurs of a" * "occurs of b" )
    freq_a = Word->Occurrences;
    tmpSgstn = Word->WordSuggestions;
    while (tmpSgstn != NULL)
    {
        freq_ab = tmpSgstn->Occurrences;
        freq_b = QueryOccurrences(Dict, tmpSgstn->WordStr);
        if (freq_a != 0 && freq_b != 0)
            tmpSgstn->Frequency = (double)freq_ab / sqrt( freq_a * freq_b );

        tmpSgstn = tmpSgstn->NextSuggestion;
    }

    return Word;
}


/* sort the frequencies of all the suggestions in the word */
Word_Type *SortAllFrequenciesInWord(Word_Type *Word)
{
    uint32_t nSgstns;
    WordSuggestion_Type *tmpSgstn;


    if (Word == NULL)
        return NULL;

    // count the number of existing suggestions
    nSgstns = 0;
    tmpSgstn = Word->WordSuggestions;
    while (tmpSgstn != NULL)
    {
        nSgstns += 1;
        tmpSgstn = tmpSgstn->NextSuggestion;
    }

    // sort the suggestions using merge sort
    Word->WordSuggestions = SortSuggestions(Word->WordSuggestions, nSgstns);

    return Word;
}


/* list all the words with its suggestions */
void DumpWord(Word_Type *Word)
{
    Word_Type *tmpWord;


    if (Word == NULL)
        return;

    // print all the words with its suggestions
    tmpWord = Word;
    while (tmpWord != NULL)
    {
        printf("%s: %d\n", tmpWord->WordStr, tmpWord->Occurrences);
        DumpSuggestions(tmpWord->WordSuggestions);
        tmpWord = tmpWord->NextWord;
    }
}


/* delete a word structure */
void DeleteWord(Word_Type *Word)
{
    WordSuggestion_Type *tmpSgstn;


    if (Word == NULL)
        return;

    // delete all the word suggestions from the word
    while (Word->WordSuggestions != NULL)
    {
        tmpSgstn = Word->WordSuggestions; // take the first suggestion in the list
        Word->WordSuggestions = Word->WordSuggestions->NextSuggestion; // point to the next suggestion
        DeleteWordSuggestion(tmpSgstn); // free the suggestion taken
    }

    free(Word->WordStr);
    free(Word);
}



/** Bucket functions **/


/* add a word to a bucket */
Bucket_Type AddWordToBucket(Bucket_Type Bkt, WordList_Type *WordInLst)
{
    Word_Type *tmpWord, *tmpLastWord;
    WordSuggestion_Type *tmpSgstn;


    if (WordInLst == NULL || WordInLst->WordStr == NULL)
        return Bkt;

    // if the bucket is empty, just add it
    if (Bkt.Words == NULL)
    {
        tmpWord = StartWord(WordInLst->WordStr);
        Bkt.Words = tmpWord;
    }
    else
    {
        // try to find the word in the bucket
        tmpWord = FindWordInBucket(Bkt, WordInLst->WordStr);
        if (tmpWord != NULL)
        {
            // if it's there, just count it once more
            tmpWord->Occurrences += 1;
        }
        else
        {
            // if it's not, find the last word in the bucket
            tmpLastWord = Bkt.Words;
            while (tmpLastWord->NextWord != NULL)
                tmpLastWord = tmpLastWord->NextWord;

            // and insert the new word after it
            tmpWord = StartWord(WordInLst->WordStr);
            tmpLastWord->NextWord = tmpWord;
        }
    }

    // add the next word in the word list as a word suggestion
    if (WordInLst->NextWord != NULL)
    {
        tmpSgstn = AddWordToWordSuggestions(tmpWord->WordSuggestions, WordInLst->NextWord->WordStr);
        tmpWord->WordSuggestions = tmpSgstn;
    }

    return Bkt;
}


/* set the frequencies of all the suggestions in the bucket */
Bucket_Type SetAllFrequenciesInBucket(Dictionary_Type *Dict, Bucket_Type Bkt)
{
    Word_Type *tmpWord;


    if (Dict == NULL)
        return Bkt;

    // update the frequencies of all the suggestions in the bucket
    tmpWord = Bkt.Words;
    while (tmpWord != NULL)
    {
        tmpWord->WordSuggestions = SetAllFrequenciesInWord(Dict, tmpWord)->WordSuggestions;
        tmpWord = tmpWord->NextWord;
    }

    return Bkt;
}


/* sort the frequencies of all the suggestions in the bucket */
Bucket_Type SortAllFrequenciesInBucket(Bucket_Type Bkt)
{
    Word_Type *tmpWord;


    // update the frequencies of all the suggestions in the bucket
    tmpWord = Bkt.Words;
    while (tmpWord != NULL)
    {
        tmpWord->WordSuggestions = SortAllFrequenciesInWord(tmpWord)->WordSuggestions;
        tmpWord = tmpWord->NextWord;
    }

    return Bkt;
}


/* list all the words in the bucket */
void PrintWordsInBucket(Bucket_Type Bkt)
{
    Word_Type *tmpWord;


    // print all the words in the bucket
    tmpWord = Bkt.Words;
    while (tmpWord != NULL)
    {
        printf("%s: %d\n", tmpWord->WordStr, tmpWord->Occurrences);
        tmpWord = tmpWord->NextWord;
    }
}


/* list all the words in the bucket with its suggestions */
void DumpBucket(Bucket_Type Bkt)
{
    Word_Type *tmpWord;


    // print all the words in the bucket with its suggestions
    tmpWord = Bkt.Words;
    while (tmpWord != NULL)
    {
        DumpWord(tmpWord);
        tmpWord = tmpWord->NextWord;
    }
}


/* delete all the words in the bucket */
void DeleteBucket(Bucket_Type Bkt)
{
    Word_Type *tmpWord;


    // delete all the words in the bucket
    while (Bkt.Words != NULL)
    {
        tmpWord = Bkt.Words; // take the first word in the list
        Bkt.Words = Bkt.Words->NextWord; // point to the next word
        DeleteWord(tmpWord); // free the word taken
    }
}



/** Dictionary functions **/


/* alocate and setup an empty dictionary */
Dictionary_Type *StartDictionary(uint32_t MaxBucketsNo)
{
    Dictionary_Type *Dict;
    uint32_t seed;


    if (MaxBucketsNo == 0)
        MaxBucketsNo = DICT_DEFAULT_MAX_BUCKET_NO; // from HashTable.h

    // generate a random seed for the hash function
    seed = rand() % 0xff;
    seed |= (rand() % 0xff) << 8;
    seed |= (rand() % 0xff) << 16;
    seed |= (rand() % 0xff) << 24;

    // return an empty allocated dictionary structure
    Dict = malloc(sizeof(Dictionary_Type));
    Dict->Buckets = calloc(MaxBucketsNo, sizeof(Bucket_Type *)); // init all the buckets to NULL pointers
    Dict->MaxBucketsNo = MaxBucketsNo;
    Dict->HashSeed = seed;
    Dict->OccupiedIndexes = NULL;
    Dict->OccupiedIndexesNo = 0;

    return Dict;
}


/* add a bucket to the list of occupied buckets */
Dictionary_Type *AddBucketToOccupiedBuckets(Dictionary_Type *Dict, Bucket_Type tmpBucket, uint32_t bucket_idx)
{
    uint32_t i, buckets;
    bool found;

    if (Dict == NULL)
        return NULL;


    // check if this bucket is already on the list of occupied buckets
    buckets = Dict->OccupiedIndexesNo;
    found = false;
    for (i = 0; i < buckets; i++)
    {
        if (bucket_idx == Dict->OccupiedIndexes[i])
        {
            found = true;
            break;
        }
    }

    // if it's a newly occupied bucket, add it to the list
    if (!found)
    {
        Dict->OccupiedIndexesNo += 1;
        Dict->OccupiedIndexes = realloc(Dict->OccupiedIndexes, Dict->OccupiedIndexesNo * sizeof(uint32_t *));
        Dict->OccupiedIndexes[Dict->OccupiedIndexesNo - 1] = bucket_idx; // add it to the end of the array
    }

    return Dict;
}


/* add a word to a dictionary */
Dictionary_Type *AddWordToDictionary(Dictionary_Type *Dict, WordList_Type *WordInLst)
{
    uint32_t bucket_idx, hash;
    Bucket_Type tmpBucket;


    if (WordInLst == NULL)
        return Dict;

    if (Dict == NULL)
        Dict = StartDictionary(DICT_DEFAULT_MAX_BUCKET_NO);

    // add the word to the bucket
    hash = HashWord(WordInLst->WordStr, Dict->HashSeed); // generate the word's hash
    bucket_idx = hash % Dict->MaxBucketsNo; // generate a table index from the hash
    tmpBucket = Dict->Buckets[bucket_idx]; // retrieve the bucket itself
    Dict->Buckets[bucket_idx] = AddWordToBucket(tmpBucket, WordInLst);
    Dict = AddBucketToOccupiedBuckets(Dict, tmpBucket, bucket_idx);

    return Dict;
}


/* add each word in a word list to the dictionary */
Dictionary_Type *AddWordListToDictionary(Dictionary_Type *Dict, WordList_Type *Lst)
{
    if (Lst == NULL)
        return Dict;

    if (Dict == NULL)
        Dict = StartDictionary(DICT_DEFAULT_MAX_BUCKET_NO);

    // add each word in the word list to the dictionary
    while (Lst != NULL)
    {
        Dict = AddWordToDictionary(Dict, Lst);
        Lst = Lst->NextWord;
    }

    return Dict;
}


/* set the frequencies of all the suggestions in the dictionary */
Dictionary_Type *SetAllFrequenciesInDictionary(Dictionary_Type *Dict)
{
    uint32_t i, buckets, bucket_idx;
    Bucket_Type tmpBucket;


    if (Dict == NULL)
        return NULL;

    // update the frequencies of all the suggestions in each occupied bucket
    buckets = Dict->OccupiedIndexesNo;
    for (i = 0; i < buckets; i++)
    {
        bucket_idx = Dict->OccupiedIndexes[i]; // get the idx of an occupied bucket
        tmpBucket = Dict->Buckets[bucket_idx]; // get an occupied bucket
        Dict->Buckets[bucket_idx] = SetAllFrequenciesInBucket(Dict, tmpBucket); // update all the frequencies in the bucket
    }

    return Dict;
}


/* sort the frequencies of all the suggestions in the dictionary */
Dictionary_Type *SortAllFrequenciesInDictionary(Dictionary_Type *Dict)
{
    uint32_t i, buckets, bucket_idx;
    Bucket_Type tmpBucket;


    if (Dict == NULL)
        return NULL;

    // update the frequencies of all the suggestions in each occupied bucket
    buckets = Dict->OccupiedIndexesNo;
    for (i = 0; i < buckets; i++)
    {
        bucket_idx = Dict->OccupiedIndexes[i]; // get the idx of an occupied bucket
        tmpBucket = Dict->Buckets[bucket_idx]; // get an occupied bucket
        Dict->Buckets[bucket_idx] = SortAllFrequenciesInBucket(tmpBucket); // update all the frequencies in the bucket
    }

    return Dict;
}


/* set and sort the frequencies of all the suggestions in the dictionary */
Dictionary_Type *SetAndSortAllFrequenciesInDictionary(Dictionary_Type *Dict)
{
    if (Dict == NULL)
        return NULL;

    Dict = SetAllFrequenciesInDictionary(Dict);
    Dict = SortAllFrequenciesInDictionary(Dict);

    return Dict;
}


/* return the number of occurrences of a word in a dictionary */
uint32_t QueryOccurrences(Dictionary_Type *Dict, const char *WordStr)
{
    uint32_t bucket_idx, hash;
    Bucket_Type tmpBucket;
    Word_Type *tmpWord;


    if (Dict == NULL || WordStr == NULL)
        return 0;

    // get the corresponding bucket
    hash = HashWord(WordStr, Dict->HashSeed); // generate the word's hash
    bucket_idx = hash % Dict->MaxBucketsNo; // generate a table index from the hash
    tmpBucket = Dict->Buckets[bucket_idx]; // retrieve the bucket itself

    // check if the word exists in the bucket
    tmpWord = FindWordInBucket(tmpBucket, WordStr);

    if(tmpWord == NULL)
        return 0;

    return tmpWord->Occurrences;
}


/* return whether or not a word exists in the dictionary */
bool QueryExistence(Dictionary_Type *Dict, const char *WordStr)
{
    bool exists;


    if (Dict == NULL || WordStr == NULL)
        return false;

    exists = (QueryOccurrences(Dict, WordStr) > 0);

    return exists;
}


/* list all the word suggestions for a given word */
void QuerySuggestions(Dictionary_Type *Dict, const char *WordStr, uint32_t nSgstns, FILE *resultsfile)
{
    uint32_t bucket_idx, hash;
    Bucket_Type tmpBucket;
    Word_Type *tmpWord;


    if (Dict == NULL || WordStr == NULL || resultsfile == NULL)
        return;

    // get the corresponding bucket
    hash = HashWord(WordStr, Dict->HashSeed); // generate the word's hash
    bucket_idx = hash % Dict->MaxBucketsNo; // generate a table index from the hash
    tmpBucket = Dict->Buckets[bucket_idx]; // retrieve the bucket itself

    // check if the word exists in the bucket
    tmpWord = FindWordInBucket(tmpBucket, WordStr);
    fprintf(resultsfile, "Query: %s\r\n", WordStr);

    // if it wasn't in the bucket, return with no suggestions
    if(tmpWord == NULL)
    {
        fprintf(resultsfile, "No suggestions\r\n");
        return;
    }

    PresentWordSuggestions(tmpWord->WordSuggestions, nSgstns, resultsfile);
}


/* query suggestions for each word in a word list */
void QuerySuggestionsFromWordList(Dictionary_Type *Dict, WordList_Type *Lst, uint32_t nSgstns, FILE *resultsfile)
{
    WordList_Type *tmpLst;


    if (Dict == NULL || Lst == NULL || resultsfile == NULL)
        return;

    // query all the words in the word list
    tmpLst = Lst;
    while (tmpLst != NULL)
    {
        QuerySuggestions(Dict, tmpLst->WordStr, nSgstns, resultsfile);
        fprintf(resultsfile, "\r\n");
        tmpLst = tmpLst->NextWord;
    }
}


/* query suggestions for each word in a text file */
void QuerySuggestionsFromTextFile(Dictionary_Type *Dict, FILE *queriesfile, uint32_t nSgstns, FILE *resultsfile)
{
    uint64_t startTimeMs, startTimeUs;
    uint32_t elapsedMs, elapsedUs;
    WordList_Type *Queries;


    if (Dict == NULL || queriesfile == NULL || resultsfile == NULL)
        return;

    // start counting time
    startTimeUs = ClockNow(CLOCK_US);
    startTimeMs = ClockNow(CLOCK_MS);

    Queries = TextFileToWordList(queriesfile);
    Queries = CaseNormalizeWordList(Queries);
    QuerySuggestionsFromWordList(Dict, Queries, nSgstns, resultsfile);
    DeleteWordList(Queries);

    // stop counting time and report it
    elapsedUs = ClockNow(CLOCK_US) - startTimeUs;
    elapsedMs = ClockNow(CLOCK_MS) - startTimeMs;
    fprintf(resultsfile, "Query time: %d milliseconds (%d microseconds)", elapsedMs, elapsedUs);
}


/* list all the words in the dictionary */
void PrintWordsInDictionary(Dictionary_Type *Dict)
{
    uint32_t i, buckets, bucket_idx;
    Bucket_Type tmpBucket;


    if (Dict == NULL)
    {
        printf("(Empty dictionary)\n");
        return;
    }

    // print all the words in each occupied bucket
    buckets = Dict->OccupiedIndexesNo;
    for (i = 0; i < buckets; i++)
    {
        bucket_idx = Dict->OccupiedIndexes[i]; // get the idx of an occupied bucket
        tmpBucket = Dict->Buckets[bucket_idx]; // get an occupied bucket
        PrintWordsInBucket(tmpBucket); // print all the words in the bucket
    }
}


/* print all the words in the dictionary with all its suggestions */
void DumpDictionary(Dictionary_Type *Dict)
{
    uint32_t i, buckets, bucket_idx;
    Bucket_Type tmpBucket;


    if (Dict == NULL)
    {
        printf("(Empty dictionary)\n");
        return;
    }

    // print all the words in each occupied bucket
    buckets = Dict->OccupiedIndexesNo;
    for (i = 0; i < buckets; i++)
    {
        bucket_idx = Dict->OccupiedIndexes[i]; // get the idx of an occupied bucket
        tmpBucket = Dict->Buckets[bucket_idx]; // get an occupied bucket
        DumpBucket(tmpBucket); // print all the words in the bucket with all its suggestions
    }
}


/* free all the hash table structures */
void DeleteDictionary(Dictionary_Type *Dict)
{
    uint32_t i, buckets, bucket_idx;
    Bucket_Type tmpBucket;


    if (Dict == NULL)
        return;

    // delete all the occupied buckets in the dictionary
    buckets = Dict->OccupiedIndexesNo;
    for (i = 0; i < buckets; i++)
    {
        bucket_idx = Dict->OccupiedIndexes[i]; // get the idx of an occupied bucket
        tmpBucket = Dict->Buckets[bucket_idx]; // get an occupied bucket
        DeleteBucket(tmpBucket); // free the bucket
    }

    // delete the dictionary structure
    free(Dict->Buckets);
    free(Dict->OccupiedIndexes);
    free(Dict);
}



/** WordList functions **/


/* init an empty word list */
WordList_Type *InitWordList(void)
{
    return NULL;
}


/* add a word to a word list */
WordList_Type *AddWordToWordList(WordList_Type *Lst, const char *WordStr)
{
    uint32_t len;
    WordList_Type *tmpNewWord, *tmpLastWord;


    if (WordStr == NULL)
        return Lst;

    // init a new word for the word list
    tmpNewWord = calloc(1, sizeof(WordList_Type));
    len = strlen(WordStr);
    tmpNewWord->WordStr = calloc(len + 1, sizeof(char)); // len + \0
    strncpy(tmpNewWord->WordStr, WordStr, len);

    // if it's the first word for this word list, just return it
    if (Lst == NULL)
        return tmpNewWord;

    // if not, find the last word in the word list
    tmpLastWord = Lst;
    while (tmpLastWord->NextWord != NULL)
        tmpLastWord = tmpLastWord->NextWord;

    // insert the new word in the word list
    tmpLastWord->NextWord = tmpNewWord;

    return Lst;
}


/* case normalize all the words in a word list */
WordList_Type *CaseNormalizeWordList(WordList_Type *Lst)
{
    WordList_Type *tmpWord;


    if (Lst == NULL)
        return NULL;

    // case normalize all the words in a word list
    tmpWord = Lst;
    while (tmpWord != NULL)
    {
        CaseNormalize(tmpWord->WordStr);
        tmpWord = tmpWord->NextWord;
    }

    return Lst;
}


/* break a line of text into a word list */
WordList_Type *TextLineToWordList(WordList_Type *Lst, const char *line)
{
    uint32_t len;
    char *tmpStr, *tmpWordTok;
    char delim[] = " !\"#$%&'()*+,-./0123456789:;<=>?@[\\]^_`{|}~\r\n\t";


    if (line == NULL)
        return Lst;

    // allocate a modifyable copy of the line for strtok
    len = strlen(line);
    tmpStr = calloc(len + 1, sizeof(char)); // len + \0
    strncpy(tmpStr, line, len);

    // break the line into words and add each to the word list
    tmpWordTok = strtok(tmpStr, delim);
    while (tmpWordTok != NULL)
    {
        Lst = AddWordToWordList(Lst, tmpWordTok);
        tmpWordTok = strtok(NULL, delim);
    }

    free(tmpStr);
    return Lst;
}


/* break a text file into a word list */
WordList_Type *TextFileToWordList(FILE *file)
{
    char *line;
    WordList_Type *tmpLst;


    if (file == NULL)
        return NULL;


    // read lines of up to 1000000 chars and break them into a word list
    line = calloc(1000000, sizeof(char)); // 1 million chars should be good enough
    tmpLst = InitWordList();
    while (fgets(line, 1000000, file) != NULL)
        tmpLst = TextLineToWordList(tmpLst, line);

    free(line);
    return tmpLst;
}


/* list all the words in a word list */
void PrintWordsInWordList(WordList_Type *Lst)
{
    WordList_Type *tmpLst;


    if (Lst == NULL)
    {
        printf("(Empty list)\n");
        return;
    }

    // print each word in the word list
    tmpLst = Lst;
    while (tmpLst != NULL)
    {
        printf("%s\n", tmpLst->WordStr);
        tmpLst = tmpLst->NextWord;
    }
}


/* free a single word list structure */
void DeleteWordInWordList(WordList_Type *Lst)
{
    if (Lst == NULL)
        return;

    free(Lst->WordStr);
    free(Lst);
}


/* free all the word list structures */
void DeleteWordList(WordList_Type *Lst)
{
    WordList_Type *tmpWordInLst;


    if (Lst == NULL)
        return;

    // delete each word from the word list
    while (Lst != NULL)
    {
        tmpWordInLst = Lst; // take the first word in the list
        Lst = Lst->NextWord; // point to the next word
        DeleteWordInWordList(tmpWordInLst); // free the word taken
    }
}
