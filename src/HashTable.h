#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define DICT_DEFAULT_MAX_BUCKET_NO 1000000 // 1 million buckets should be good enough

typedef struct __WordSuggestion_Struct
{
    char *WordStr;
    uint32_t Occurrences;
    double Frequency;
    struct __WordSuggestion_Struct *NextSuggestion;
} WordSuggestion_Type;

typedef struct __Word_Struct
{
    char *WordStr;
    uint32_t Occurrences;
    WordSuggestion_Type *WordSuggestions;
    struct __Word_Struct *NextWord;
} Word_Type;

typedef struct
{
    Word_Type *Words;
} Bucket_Type;

typedef struct
{
    Bucket_Type *Buckets;
    uint32_t MaxBucketsNo;
    uint32_t HashSeed;
    uint32_t OccupiedIndexesNo;
    uint32_t *OccupiedIndexes;
} Dictionary_Type;

typedef struct __WordList_Struct
{
    char *WordStr;
    struct __WordList_Struct *NextWord;
} WordList_Type;


/* General purpose functions */
void CaseNormalize(char *str);
uint32_t HashWord(const char *str, uint32_t seed);

/* WordSuggestion functions */
WordSuggestion_Type *FindSuggestionInSuggestionsList(WordSuggestion_Type *Sgstn, const char *WordStr);
WordSuggestion_Type *AddWordToWordSuggestions(WordSuggestion_Type *Sgstn, const char *WordStr);
WordSuggestion_Type *MergeSuggestions(WordSuggestion_Type *SgstnPart1, WordSuggestion_Type *SgstnPart2);
WordSuggestion_Type *SortSuggestions(WordSuggestion_Type *Sgstn, uint32_t nSgstns);
void DumpSuggestions(WordSuggestion_Type *Sgstn);
void PresentWordSuggestions(WordSuggestion_Type *Sgstn, uint32_t nSgstns, FILE *resultsfile);
void DeleteWordSuggestion(WordSuggestion_Type *Sgstn);

/* Word functions */
Word_Type *StartWord(const char *WordStr);
Word_Type *FindWordInBucket(Bucket_Type Bkt, const char *WordStr);
Word_Type *SetAllFrequenciesInWord(Dictionary_Type *Dict, Word_Type *Word);
Word_Type *SortAllFrequenciesInWord(Word_Type *Word);
void DumpWord(Word_Type *Word);
void DeleteWord(Word_Type *Word);

/* Bucket functions */
Bucket_Type AddWordToBucket(Bucket_Type Bkt, WordList_Type *WordInLst);
Bucket_Type SetAllFrequenciesInBucket(Dictionary_Type *Dict, Bucket_Type Bkt);
Bucket_Type SortAllFrequenciesInBucket(Bucket_Type Bkt);
void PrintWordsInBucket(Bucket_Type Bkt);
void DumpBucket(Bucket_Type Bkt);
void DeleteBucket(Bucket_Type Bkt);

/* Dictionary functions */
Dictionary_Type *StartDictionary(uint32_t MaxBucketsNo);
Dictionary_Type *AddBucketToOccupiedBuckets(Dictionary_Type *Dict, Bucket_Type tmpBucket, uint32_t bucket_idx);
Dictionary_Type *AddWordToDictionary(Dictionary_Type *Dict, WordList_Type *WordInLst);
Dictionary_Type *AddWordListToDictionary(Dictionary_Type *Dict, WordList_Type *Lst);
Dictionary_Type *SetAllFrequenciesInDictionary(Dictionary_Type *Dict);
Dictionary_Type *SortAllFrequenciesInDictionary(Dictionary_Type *Dict);
Dictionary_Type *SetAndSortAllFrequenciesInDictionary(Dictionary_Type *Dict);
uint32_t QueryOccurrences(Dictionary_Type *Dict, const char *WordStr);
bool QueryExistence(Dictionary_Type *Dict, const char *WordStr);
void QuerySuggestions(Dictionary_Type *Dict, const char *WordStr, uint32_t nSgstns, FILE *resultsfile);
void QuerySuggestionsFromWordList(Dictionary_Type *Dict, WordList_Type *Lst, uint32_t nSgstns, FILE *resultsfile);
void QuerySuggestionsFromTextFile(Dictionary_Type *Dict, FILE *queriesfile, uint32_t nSgstns, FILE *resultsfile);
void PrintWordsInDictionary(Dictionary_Type *Dict);
void DumpDictionary(Dictionary_Type *Dict);
void DeleteDictionary(Dictionary_Type *Dict);

/* WordList functions */
WordList_Type *InitWordList(void);
WordList_Type *AddWordToWordList(WordList_Type *Lst, const char *WordStr);
WordList_Type *CaseNormalizeWordList(WordList_Type *Lst);
WordList_Type *TextLineToWordList(WordList_Type *Lst, const char *line);
WordList_Type *TextFileToWordList(FILE *file);
void PrintWordsInWordList(WordList_Type *Lst);
void DeleteWordInWordList(WordList_Type *Lst);
void DeleteWordList(WordList_Type *Lst);

#endif // _HASHTABLE_H_
